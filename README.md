# eShop

Technologies:
- PHP 7.3.4
- Symfony 4.4.4
- MySQL 5.7

Folder `src` is divided into 4 contexts: `Api`, `Auth`, `Shop`, `Shared`.
Each context can be divided into 3 layers: `Domain`, `Application`, `Infrastructure`.
Some Contexts have only layers required to that project .

`Api` context contains united api endpoints for project.

`Auth` context is responsible for registration and authentication

`Shop` context contains the required logic for `Product`

`Shared` context contains ValueObjects and Services that can be used in other contexts.

Api:
 - `POST /api/products ` Create product (admin access)
    ```
       {
            "name": string, 
            "description": string, 
            "prices": [
                {
                    "price": float, 
                    "currency": string
                }
            ]
       }
   ```
 - `GET /api/products/{id}` Get product
 - `GET /api/products` Get product list (with pagination)
    - Available query parameters
   ```
        _order: ASC/DESC
        _offset: int
        _limit: int
   ```
    - Pagination response block
   ```
    {
        "_pagination": {
            "total": int,
            "offset": int,
            "limit": int
        }
    }
   ``` 
   
 - `POST /api/users` Create user
   ```
       "email": string,
       "password": string
   ```

Auth:
 - `GET /auth/registration` Registration Page
 - `GET/POST /auth/login` Login Page/Action
 - `GET /auth/logout` Logout

Shop:
 - `GET /shop/product` Create product Page (admin access)
 
How to run:
 - install composer dependencies `composer install —ignore-platform-reqs`
 - Copy .env file `cp .env.local .env`
 - `docker-compose up -d`
 - `docker-compose exec app bin/console doctrine:schema:update --force`

Tests
 - Copy PhpUnit configuration `cp .phpunit.xml.dist .phpunit.xml`
 - `docker-compose exec app vendor/bin/phpunit  --configuration phpunit.xml tests`

Was planned, but not released:
 - unit tests for domain and services
 - test of mail sending (using Mailhog)
 - required UI
 - dto insted of `toArray` entity method
 
 User fixtures contain admin user with login `admin@test.com` and password `SecretPassword`
 Mails are available on `8025` application port
