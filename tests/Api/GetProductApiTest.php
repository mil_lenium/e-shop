<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\Auth\Infrastructure\DataFixtures\UserFixtures;
use App\Shared\Infrastructure\Test\ExtendedTestCase;
use App\Shop\Infrastructure\DataFixtures\ProductFixtures;

class GetProductApiTest extends ExtendedTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->addFixture(new ProductFixtures());
        $this->addFixture(new UserFixtures());
        $this->resetDatabase();
        parent::ensureKernelShutdown();
    }

    public function testResponseProductStructure(): void
    {
        $response = $this->request('/api/products/1', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $response = $this->getResponseContent($response);
        self::assertArrayHasKey('product', $response);

        $product = $response['product'];
        $prices = $response['product']['prices'];
        unset($product['prices'], $prices[0]['id'], $prices[1]['id']);

        $description = [
            'That description is nice. It should contains at least 100 symbols.',
            'It is awesome. Few symbols more please.',
        ];

        self::assertSame([
            'id' => '1',
            'name' => 'Name 1',
            'description' => implode(' ', $description),
            'createdAt' => '2020-01-01 12:00:00',
        ], $product);

        self::assertSame([
            [
                'productId' => '1',
                'price' => 6,
                'currency' => 'pln',
            ],
            [
                'productId' => '1',
                'price' => 1.5,
                'currency' => 'eur',
            ],
        ], $prices);
    }

    public function testNotFoundIfProductNotExists(): void
    {
        $response = $this->request('/api/products/not-existent', 'GET');
        self::assertSame(404, $response->getStatusCode());
    }

    public function testResponsePaginationStructure(): void
    {
        $response = $this->request('/api/products', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $pagination = $this->getResponseContent($response)['_pagination'];
        self::assertSame([
            'total' => 20,
            'offset' => 0,
            'limit' => 10,
        ], $pagination);
    }

    public function testGetProductsContainsProducts(): void
    {
        $response = $this->request('/api/products', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $response = $this->getResponseContent($response);
        self::assertArrayHasKey('products', $response);
        self::assertCount(10, $response['products']);
    }

    public function testGetProductsContainsPagination(): void
    {
        $response = $this->request('/api/products', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $response = $this->getResponseContent($response);
        self::assertArrayHasKey('_pagination', $response);
    }

    public function testPaginationLimit(): void
    {
        $response = $this->request('/api/products?_limit=2', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $response = $this->getResponseContent($response);
        self::assertCount(2, $response['products']);
    }

    public function testPaginationOffset(): void
    {
        $response = $this->request('/api/products?_offset=18_limit=4', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $response = $this->getResponseContent($response);
        self::assertCount(2, $response['products']);
    }

    public function testPaginationASCOrder(): void
    {
        $response = $this->request('/api/products?_limit=1&_order=asc', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $response = $this->getResponseContent($response);

        $products = $response['products'];
        unset($products[0]['prices'], $products[1]['prices']);

        $description = [
            'That description is nice. It should contains at least 100 symbols.',
            'It is awesome. Few symbols more please.',
        ];

        self::assertSame([
            [
                'id' => '1',
                'name' => 'Name 1',
                'description' => implode(' ', $description),
                'createdAt' => '2020-01-01 12:00:00',
            ],
        ], $products);
    }

    public function testPaginationDescOrderIfInvalidOrderProvided(): void
    {
        $response = $this->request('/api/products?_limit=1&_order=invalid', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $response = $this->getResponseContent($response);

        $products = $response['products'];
        unset($products[0]['prices'], $products[1]['prices']);

        $description = [
            'That description is nice. It should contains at least 100 symbols.',
            'It is awesome. Few symbols more please.',
        ];

        self::assertSame([
            [
                'id' => '20',
                'name' => 'Name 20',
                'description' => implode(' ', $description),
                'createdAt' => '2020-20-01 12:00:00',
            ],
        ], $products);
    }

    public function testResponsePaginationChangesAccordingToQuery(): void
    {
        $response = $this->request('/api/products?_limit=10&_offset=15', 'GET');
        self::assertSame(200, $response->getStatusCode());

        $pagination = $this->getResponseContent($response)['_pagination'];
        self::assertSame([
            'total' => 20,
            'offset' => 15,
            'limit' => 10,
        ], $pagination);
    }
}
