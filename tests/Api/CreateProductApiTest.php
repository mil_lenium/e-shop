<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\Shared\Infrastructure\Test\ExtendedTestCase;

class CreateProductApiTest extends ExtendedTestCase
{
    public function testCreateProductRedirectsIfNotAuthenticated(): void
    {
        $description = [
            'That description is nice. It should contains at least 100 symbols. ',
            'It is awesome. Few symbols more please.',
        ];
        $body = [
            'name' => 'name',
            'description' => implode('', $description),
            'prices' => [
                [
                    'price' => 100.50,
                    'currency' => 'PLN',
                ],
                [
                    'price' => 22.15,
                    'currency' => 'USD',
                ],
            ],
        ];

        $response = $this->request('/api/products', 'POST', $body);
        self::assertSame(302, $response->getStatusCode());
    }
}
