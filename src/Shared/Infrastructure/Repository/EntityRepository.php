<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Repository\EntityRepositoryInterface;
use Doctrine\ORM\EntityRepository as BaseEntityRepository;

abstract class EntityRepository extends BaseEntityRepository implements EntityRepositoryInterface
{
    protected function saveEntity($entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }
}
