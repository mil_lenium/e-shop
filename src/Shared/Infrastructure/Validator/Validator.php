<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Validator;

use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class Validator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    protected function throwIfNotValid($value, Constraint $constraint, string $property): void
    {
        $errors = $this->validator->validate($value, $constraint);

        foreach ($errors as $error) {
            throw new Exception("Property: [{$property}]. Message: {$error->getMessage()}");
        }
    }
}
