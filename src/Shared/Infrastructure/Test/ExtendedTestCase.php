<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Test;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExtendedTestCase extends WebTestCase
{
    /**
     * @var ORMExecutor
     */
    private $fixtureExecutor;

    /**
     * @var ContainerAwareLoader
     */
    private $fixtureLoader;

    protected function setUp(): void
    {
        self::bootKernel();
    }

    protected function addFixture(FixtureInterface $fixture): void
    {
        $this->getFixtureLoader()->addFixture($fixture);
    }

    protected function resetDatabase(): void
    {
        $this->getFixtureExecutor()->execute($this->getFixtureLoader()->getFixtures());
    }

    private function getFixtureExecutor(): ORMExecutor
    {
        if (!$this->fixtureExecutor) {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = self::$kernel->getContainer()->get('doctrine')->getManager();
            $this->fixtureExecutor = new ORMExecutor($entityManager, new ORMPurger($entityManager));
        }

        return $this->fixtureExecutor;
    }

    private function getFixtureLoader(): ContainerAwareLoader
    {
        if (!$this->fixtureLoader) {
            $this->fixtureLoader = new ContainerAwareLoader(self::$kernel->getContainer());
        }

        return $this->fixtureLoader;
    }

    public function request(string $path, string $method, array $body = [], array $headers = []): ResponseInterface
    {
        $baseUri = getenv('APP_URI');
        $client = new Client(['http_errors' => false, 'allow_redirects' => false]);

        return $client->request($method, $baseUri.$path, [
            'headers' => $headers,
            'body' => json_encode($body, JSON_THROW_ON_ERROR),
        ]);
    }

    public function getResponseContent(ResponseInterface $response): array
    {
        return json_decode((string) $response->getBody(), true, JSON_THROW_ON_ERROR) ?? [];
    }
}
