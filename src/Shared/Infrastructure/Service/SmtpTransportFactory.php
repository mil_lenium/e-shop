<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service;

use Swift_SmtpTransport;
use Swift_Transport;

class SmtpTransportFactory implements TransportFactoryInterface
{
    public function create(string $host, int $port, ?string $encryption): Swift_Transport
    {
        return new Swift_SmtpTransport($host, $port, $encryption);
    }
}
