<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service;

use App\Shared\Domain\ValueObject\MessageInterface;

interface MailSenderInterface
{
    public function send(MessageInterface $message): void;
}
