<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;

interface RequestContentProviderInterface
{
    public function provide(Request $request): array;
}
