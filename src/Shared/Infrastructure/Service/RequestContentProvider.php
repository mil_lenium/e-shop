<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service;

use Symfony\Component\HttpFoundation\Request;

class RequestContentProvider implements RequestContentProviderInterface
{
    public function provide(Request $request): array
    {
        return json_decode($request->getContent(), true, JSON_THROW_ON_ERROR);
    }
}
