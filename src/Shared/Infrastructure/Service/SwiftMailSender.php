<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service;

use App\Shared\Domain\ValueObject\MessageInterface;
use App\Shop\Domain\Service\Notifier;
use Swift_Mailer;
use Swift_Message;
use Swift_Transport;

class SwiftMailSender implements MailSenderInterface, Notifier
{
    /**
     * @var Swift_Transport
     */
    private $transport;

    public function __construct(Swift_Transport $transport)
    {
        $this->transport = $transport;
    }

    public function notify(MessageInterface $message): void
    {
        $this->send($message);
    }

    public function send(MessageInterface $message): void
    {
        $swiftMessage = (new Swift_Message())
            ->setSubject($message->getSubject())
            ->setFrom($message->getSender())
            ->setTo($message->getReceiver())
            ->setBody($message->getMessage());

        $mailer = new Swift_Mailer($this->transport);
        $mailer->send($swiftMessage);
    }
}
