<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Service;

use Swift_Transport;

interface TransportFactoryInterface
{
    public function create(string $host, int $port, ?string $encryption): Swift_Transport;
}
