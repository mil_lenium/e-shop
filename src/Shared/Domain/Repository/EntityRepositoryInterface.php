<?php

declare(strict_types=1);

namespace App\Shared\Domain\Repository;

interface EntityRepositoryInterface
{
    public function count(array $criteria);
}
