<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

use App\Shop\Domain\ValueObject\ValueObject;
use Ramsey\Uuid\Uuid as RamseyUuid;

class Uuid implements ValueObject
{
    /**
     * @var string
     */
    private $id;

    public function fromString(string $id): self
    {
        return new self($id);
    }

    public static function random(): self
    {
        return new self(RamseyUuid::uuid4()->toString());
    }

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function toString(): string
    {
        return $this->id;
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->toString() === $this->toString();
        }

        return false;
    }
}
