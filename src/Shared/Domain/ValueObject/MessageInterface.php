<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

interface MessageInterface
{
    public function getSubject(): string;

    public function getSender(): string;

    public function getReceiver(): string;

    public function getMessage(): string;
}
