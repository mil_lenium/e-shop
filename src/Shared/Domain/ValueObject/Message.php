<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

use App\Shop\Domain\ValueObject\ValueObject;

final class Message implements ValueObject, MessageInterface
{
    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $sender;

    /**
     * @var string
     */
    private $receiver;

    /**
     * @var string
     */
    private $message;

    public static function create(string $subject, string $sender, string $receiver, string $message): self
    {
        return new self($subject, $sender, $receiver, $message);
    }

    private function __construct(string $subject, string $sender, string $receiver, string $message)
    {
        $this->subject = $subject;
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->message = $message;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getSender(): string
    {
        return $this->sender;
    }

    public function getReceiver(): string
    {
        return $this->receiver;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->getSubject() === $this->getSubject()
                && $another->getSender() === $this->getSender()
                && $another->getReceiver() === $this->getReceiver()
                && $another->getMessage() === $this->getMessage();
        }

        return false;
    }
}
