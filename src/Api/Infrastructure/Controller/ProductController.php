<?php

declare(strict_types=1);

namespace App\Api\Infrastructure\Controller;

use App\Shared\Infrastructure\Service\RequestContentProviderInterface;
use App\Shop\Infrastructure\Processor\CreateProductProcessorInterface;
use App\Shop\Infrastructure\Processor\GetProductProcessorInterface;
use App\Shop\Infrastructure\Processor\GetProductsProcessorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractController
{
    /**
     * @var CreateProductProcessorInterface
     */
    private $createProductProcessor;

    /**
     * @var GetProductsProcessorInterface
     */
    private $getProductsProcessor;

    /**
     * @var GetProductProcessorInterface
     */
    private $getProductProcessor;

    /**
     * @var RequestContentProviderInterface
     */
    private $requestContentProvider;

    public function __construct(
        CreateProductProcessorInterface $createProductProcessor,
        GetProductsProcessorInterface $getProductsProcessor,
        GetProductProcessorInterface $getProductProcessor,
        RequestContentProviderInterface $requestContentProvider
    ) {
        $this->createProductProcessor = $createProductProcessor;
        $this->getProductsProcessor = $getProductsProcessor;
        $this->getProductProcessor = $getProductProcessor;
        $this->requestContentProvider = $requestContentProvider;
    }

    public function createProduct(Request $request): JsonResponse
    {
        $content = $this->requestContentProvider->provide($request);
        $product = $this->createProductProcessor->process($content);
        return JsonResponse::create($product->toArray(), JsonResponse::HTTP_CREATED);
    }

    public function getProducts(Request $request): JsonResponse
    {
        $products = $this->getProductsProcessor->process($request);
        return JsonResponse::create($products);
    }

    public function getProduct(string $id): JsonResponse
    {
        $product = $this->getProductProcessor->process($id);

        if (null === $product) {
            return new JsonResponse([], JsonResponse::HTTP_NOT_FOUND);
        }

        return JsonResponse::create(['product' => $product->toArray()]);
    }
}
