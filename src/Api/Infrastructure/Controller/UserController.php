<?php

declare(strict_types=1);

namespace App\Api\Infrastructure\Controller;

use App\Auth\Infrastructure\Processor\Registration\RegistrationProcessorInterface;
use App\Shared\Infrastructure\Service\RequestContentProviderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController
{
    /**
     * @var RequestContentProviderInterface
     */
    private $requestContentProvider;

    /**
     * @var RegistrationProcessorInterface
     */
    private $registrationProcessor;

    public function __construct(
        RequestContentProviderInterface $requestContentProvider,
        RegistrationProcessorInterface $registrationProcessor
    ) {
        $this->requestContentProvider = $requestContentProvider;
        $this->registrationProcessor = $registrationProcessor;
    }

    public function createUser(Request $request): JsonResponse
    {
        $data = $this->requestContentProvider->provide($request);
        $this->registrationProcessor->process($data);

        return JsonResponse::create([], JsonResponse::HTTP_CREATED);
    }
}
