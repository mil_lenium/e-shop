<?php

declare(strict_types=1);

namespace App\Auth\Domain\Repository;

use App\Auth\Domain\Model\UserInterface;
use App\Auth\Domain\ValueObject\Username;
use App\Shared\Domain\Repository\EntityRepositoryInterface;

interface UserRepositoryInterface extends EntityRepositoryInterface
{
    public function save(UserInterface $user): void;

    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void;

    public function findOneByUsername(Username $username): ?UserInterface;
}
