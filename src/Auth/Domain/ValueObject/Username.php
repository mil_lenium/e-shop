<?php

declare(strict_types=1);

namespace App\Auth\Domain\ValueObject;

use App\Shop\Domain\ValueObject\ValueObject;

final class Username implements ValueObject
{
    /**
     * @var string
     */
    private $username;

    public static function fromString(string $username): self
    {
        return new self($username);
    }

    private function __construct(string $username)
    {
        $this->username = $username;
    }

    public function toString(): string
    {
        return $this->username;
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->toString() === $this->toString();
        }

        return false;
    }
}
