<?php

declare(strict_types=1);

namespace App\Auth\Domain\Factory;

use App\Auth\Domain\Model\UserInterface;
use App\Auth\Domain\ValueObject\Username;

interface UserFactoryInterface
{
    public function create(Username $username, string $plainPassword): UserInterface;
}
