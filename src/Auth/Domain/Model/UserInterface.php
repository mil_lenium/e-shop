<?php

declare(strict_types=1);

namespace App\Auth\Domain\Model;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

interface UserInterface extends BaseUserInterface
{
    public function getId(): ?int;

    public function getRoles(): array;

    public function getPassword(): string;

    public function getUsername(): string;
}
