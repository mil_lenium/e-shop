<?php

declare(strict_types=1);

namespace App\Auth\Domain\Model;

use Serializable;

class User implements UserInterface, Serializable
{
    public const USER_ROLE = 'ROLE_USER';
    public const ADMIN_ROLE = 'ROLE_ADMIN';

    private $id;

    private $email;

    private $roles = [];

    private $password;

    /**
     * @var string
     */
    private $salt;

    public function __construct()
    {
        $this->salt = uniqid((string) mt_rand(), true);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function serialize()
    {
        return serialize([
            $this->getId(),
            $this->getUsername(),
            $this->getPassword(),
            $this->getSalt(),
        ]);
    }

    public function unserialize($serialized): void
    {
        list(
            $this->id,
            $this->email,
            $this->password,
            $this->salt
            ) = unserialize($serialized);
    }

    public function eraseCredentials(): void
    {
    }
}
