<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\Repository;

use App\Auth\Domain\Model\User;
use App\Auth\Domain\Model\UserInterface;
use App\Auth\Domain\Repository\UserRepositoryInterface;
use App\Auth\Domain\ValueObject\Username;
use App\Shared\Infrastructure\Repository\EntityRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserRepository extends EntityRepository implements UserRepositoryInterface
{
    public function save(UserInterface $user): void
    {
        $this->saveEntity($user);
    }

    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            $message = sprintf('Instances of "%s" are not supported.', get_class($user));
            throw new UnsupportedUserException($message);
        }

        $user->setPassword($newEncodedPassword);
        $this->saveEntity($user);
    }

    public function findOneByUsername(Username $username): ?UserInterface
    {
        return $this->findOneBy(['email' => $username->toString()]);
    }
}
