<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\Processor\Registration;

interface RegistrationProcessorInterface
{
    public function process(array $data): void;
}
