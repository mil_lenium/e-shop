<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\Processor\Registration;

use App\Auth\Domain\Factory\UserFactoryInterface;
use App\Auth\Domain\Repository\UserRepositoryInterface;
use App\Auth\Domain\ValueObject\Username;
use App\Auth\Infrastructure\Validator\RegisterValidatorInterface;
use Exception;

class RegistrationProcessor implements RegistrationProcessorInterface
{
    /**
     * @var UserFactoryInterface
     */
    private $userFactory;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var RegisterValidatorInterface
     */
    private $validator;

    public function __construct(
        UserFactoryInterface $userFactory,
        UserRepositoryInterface $userRepository,
        RegisterValidatorInterface $validator
    ) {
        $this->userFactory = $userFactory;
        $this->userRepository = $userRepository;
        $this->validator = $validator;
    }

    public function process(array $data): void
    {
        $this->validator->validate($data);

        $username = Username::fromString($data['email']);
        $plainPassword = $data['password'];
        $user = $this->userRepository->findOneByUsername($username);

        if (null !== $user) {
            throw new Exception("User with username [{$username->toString()}] already exists.");
        }

        $user = $this->userFactory->create($username, $plainPassword);
        $this->userRepository->save($user);
    }
}
