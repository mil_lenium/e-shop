<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\Validator;

interface RegisterValidatorInterface
{
    public function validate(array $data): void;
}
