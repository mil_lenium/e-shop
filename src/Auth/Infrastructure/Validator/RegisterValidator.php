<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\Validator;

use App\Shared\Infrastructure\Validator\Validator;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterValidator extends Validator implements RegisterValidatorInterface
{
    public function validate(array $data): void
    {
        $this->validateEmail($data['email'] ?? null);
        $this->validatePassword($data['password'] ?? null);
    }

    private function validateEmail(?string $email): void
    {
        $this->throwIfNotValid($email, new NotBlank(), 'email');
        $this->throwIfNotValid($email, new Email(), 'email');
    }

    private function validatePassword(?string $password): void
    {
        $this->throwIfNotValid($password, new NotBlank(), 'password');
        $this->throwIfNotValid($password, new Length(['min' => 8]), 'password');
    }
}
