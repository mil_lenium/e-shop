<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\DataFixtures;

use App\Auth\Domain\Model\User;
use App\Auth\Domain\Model\UserInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->persist($this->createAdmin());
        $manager->persist($this->createUser());
        $manager->flush();
    }

    private function createAdmin(): UserInterface
    {
        $password = [
            '$argon2id$v=19$m=65536,t=4,',
            'p=1$8AvbCDn8ZhXsidY0xqrV+g$e88GkoVFFYBE8gkJYn+UaU3uW6aSHBGz3bMSCvr9n4I',
        ];

        return (new User())
            ->setEmail('admin@test.com')
            ->setPassword(implode('', $password))
            ->setRoles([User::USER_ROLE, User::ADMIN_ROLE]);
    }

    private function createUser(): UserInterface
    {
        $password = [
            '$argon2id$v=19$m=65536,t=4,',
            'p=1$0KZuwD/TfgVhBITRpT4I0g$YglyrCb4YzjfWhy4Xbnw2oP9X+vru5b7nFOlFdf0Ss8',
        ];

        return (new User())
            ->setEmail('user@test.com')
            ->setPassword(implode('', $password))
            ->setRoles([User::USER_ROLE]);
    }
}
