<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\Controller;

use App\Auth\Infrastructure\Form\UserType;
use App\Auth\Infrastructure\Processor\Registration\RegistrationProcessorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends AbstractController
{
    /**
     * @var RegistrationProcessorInterface
     */
    private $registrationProcessor;

    public function __construct(RegistrationProcessorInterface $registrationProcessor)
    {
        $this->registrationProcessor = $registrationProcessor;
    }

    public function registration()
    {
        $form = $this->createForm(UserType::class);

        return $this->render('auth/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function user(Request $request): JsonResponse
    {
        $data = $request->request->get('user');
        $this->registrationProcessor->process($data);

        return JsonResponse::create([], JsonResponse::HTTP_CREATED);
    }
}
