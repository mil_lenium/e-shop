<?php

declare(strict_types=1);

namespace App\Auth\Infrastructure\Factory;

use App\Auth\Domain\Factory\UserFactoryInterface;
use App\Auth\Domain\Model\User;
use App\Auth\Domain\Model\UserInterface;
use App\Auth\Domain\ValueObject\Username;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserFactory implements UserFactoryInterface
{
    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function create(Username $username, string $plainPassword): UserInterface
    {
        $user = new User();

        $encoder = $this->encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
        $user->setEmail($username->toString())
            ->setPassword($password)
            ->setRoles(['ROLE_USER']);

        return $user;
    }
}
