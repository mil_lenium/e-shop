<?php

declare(strict_types=1);

namespace App\Shop\Domain\Service;

use App\Shared\Domain\ValueObject\MessageInterface;

interface Notifier
{
    public function notify(MessageInterface $message): void;
}
