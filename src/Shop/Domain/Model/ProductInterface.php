<?php

declare(strict_types=1);

namespace App\Shop\Domain\Model;

use App\Shop\Domain\ValueObject\Currency;
use App\Shop\Domain\ValueObject\ProductDescription;
use App\Shop\Domain\ValueObject\ProductId;
use App\Shop\Domain\ValueObject\ProductName;
use DateTime;

interface ProductInterface
{
    public function getId(): ProductId;

    public function getName(): ProductName;

    public function setName(ProductName $name): void;

    public function getDescription(): ProductDescription;

    public function setDescription(ProductDescription $description): void;

    public function getCreatedAt(): DateTime;

    public function addPrice(ProductPriceInterface $price): void;

    public function removePrice(Currency $currency): void;

    public function getPrices(): array;

    public function toArray(): array;
}
