<?php

declare(strict_types=1);

namespace App\Shop\Domain\Model;

use App\Shop\Domain\ValueObject\Currency;
use App\Shop\Domain\ValueObject\Price;

interface ProductPriceInterface
{
    public function getId(): string;

    public function getProduct(): ProductInterface;

    public function setProduct(ProductInterface $product): void;

    public function getPrice(): Price;

    public function setPrice(Price $price): void;

    public function getCurrency(): Currency;

    public function setCurrency(Currency $currency): void;

    public function toArray(): array;
}
