<?php

declare(strict_types=1);

namespace App\Shop\Domain\Model;

use App\Shared\Domain\ValueObject\Uuid;
use App\Shop\Domain\ValueObject\Currency;
use App\Shop\Domain\ValueObject\Price;

class ProductPrice implements ProductPriceInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var ProductInterface
     */
    private $product;

    /**
     * @var Price
     */
    private $price;

    /**
     * @var Currency
     */
    private $currency;

    public function __construct(ProductInterface $product, Price $price, Currency $currency)
    {
        $this->id = Uuid::random()->toString();
        $this->product = $product;
        $this->price = $price;
        $this->currency = $currency;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    public function setProduct(ProductInterface $product): void
    {
        $this->product = $product;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): void
    {
        $this->price = $price;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'productId' => $this->getProduct()->getId()->toString(),
            'price' => $this->getPrice()->toFloat(),
            'currency' => $this->getCurrency()->toString(),
        ];
    }
}
