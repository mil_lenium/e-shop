<?php

declare(strict_types=1);

namespace App\Shop\Domain\Model;

use App\Shop\Domain\ValueObject\Currency;
use App\Shop\Domain\ValueObject\ProductDescription;
use App\Shop\Domain\ValueObject\ProductId;
use App\Shop\Domain\ValueObject\ProductName;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;

class Product implements ProductInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var ProductName
     */
    private $name;

    /**
     * @var ProductDescription
     */
    private $description;

    /**
     * @var ArrayCollection|ProductPriceInterface[]
     */
    private $prices;

    /**
     * @var DateTime
     */
    private $createdAt;

    public function __construct(
        ProductName $name,
        ProductDescription $description,
        DateTime $createdAt,
        ProductId $id = null
    ) {
        $this->id = ($id ?? ProductId::random())->toString();
        $this->name = $name;
        $this->description = $description;
        $this->prices = new ArrayCollection();
        $this->createdAt = $createdAt;
    }

    public function getId(): ProductId
    {
        return ProductId::fromString($this->id);
    }

    public function getName(): ProductName
    {
        return $this->name;
    }

    public function setName(ProductName $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ProductDescription
    {
        return $this->description;
    }

    public function setDescription(ProductDescription $description): void
    {
        $this->description = $description;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return ProductPrice[]
     */
    public function getPrices(): array
    {
        return $this->prices->toArray();
    }

    public function setPrices(array $prices): void
    {
        $this->prices = new ArrayCollection();

        foreach ($prices as $price) {
            $this->addPrice($price);
        }
    }

    public function addPrice(ProductPriceInterface $price): void
    {
        foreach ($this->prices as $existentPrice) {
            if ($existentPrice->getCurrency()->equals($price->getCurrency())) {
                $message = "Product [{$this->getName()->toString()}] already has [{$price->getCurrency()->toString()}] price.";
                throw new Exception($message);
            }
        }

        $this->prices[] = $price;
    }

    public function removePrice(Currency $currency): void
    {
        foreach ($this->prices as $existentPrice) {
            if ($existentPrice->getCurrency()->equals($currency)) {
                $this->prices->removeElement($existentPrice);
            }
        }
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId()->toString(),
            'name' => $this->getName()->toString(),
            'description' => $this->getDescription()->toString(),
            'createdAt' => $this->getCreatedAt()->format('Y-d-m H:i:s'),
            'prices' => array_map(function (ProductPriceInterface $price) {
                return $price->toArray();
            }, $this->getPrices()),
        ];
    }
}
