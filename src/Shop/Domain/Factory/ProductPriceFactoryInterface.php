<?php

declare(strict_types=1);

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\Model\ProductPriceInterface;

interface ProductPriceFactoryInterface
{
    public function create(ProductInterface $product, array $data): ProductPriceInterface;
}
