<?php

declare(strict_types=1);

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\Model\ProductInterface;

interface ProductFactoryInterface
{
    public function create(array $data): ProductInterface;
}
