<?php

declare(strict_types=1);

namespace App\Shop\Domain\Repository;

use App\Shared\Domain\Repository\EntityRepositoryInterface;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\ValueObject\PaginationConfigurationInterface;
use App\Shop\Domain\ValueObject\ProductId;

interface ProductRepositoryInterface extends EntityRepositoryInterface
{
    public function save(ProductInterface $product): void;

    public function findOneById(ProductId $id): ?ProductInterface;

    public function findList(PaginationConfigurationInterface $configuration): array;
}
