<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

final class Nil implements ValueObject
{
    public function __call($name, $arguments): void
    {
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return true;
        }

        return false;
    }
}
