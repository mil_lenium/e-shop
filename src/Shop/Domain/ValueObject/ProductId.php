<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

use App\Shared\Domain\ValueObject\Uuid;

final class ProductId implements ValueObject
{
    /**
     * @var string
     */
    private $id;

    public static function fromString(string $id): self
    {
        return new self($id);
    }

    public static function random(): self
    {
        return new self(Uuid::random()->toString());
    }

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public function toString(): string
    {
        return $this->id;
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->toString() === $this->toString();
        }

        return false;
    }
}
