<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

final class Price implements ValueObject
{
    private const ZERO = 0;

    /**
     * @var float
     */
    private $value;

    public static function fromFloat(float $price): self
    {
        return new self($price);
    }

    private function __construct(float $price)
    {
        $this->value = $price;
    }

    public function toFloat(): float
    {
        return $this->value;
    }

    public function add(Price $price): self
    {
        $result = $this->toFloat() + $price->toFloat();
        return new self($result);
    }

    public function subtract(Price $price): self
    {
        $result = $this->toFloat() - $price->toFloat();
        return new self($result);
    }

    public function multiply(Price $price): self
    {
        $result = $this->toFloat() * $price->toFloat();
        return new self($result);
    }

    public function divide(Price $price): self
    {
        if (self::ZERO === $price->toFloat()) {
            return new self(self::ZERO);
        }

        $result = $this->toFloat() / $price->toFloat();
        return new self($result);
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->toFloat() === $this->toFloat();
        }

        return false;
    }
}
