<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

use Exception;

final class ProductDescription implements ValueObject
{
    private const MIN_DESCRIPTION_LENGTH = 100;

    /**
     * @var string
     */
    private $description;

    public static function fromString(string $description): self
    {
        if (self::MIN_DESCRIPTION_LENGTH > strlen($description)) {
            throw new Exception('Provided description has less than 100 chars');
        }

        return new self($description);
    }

    private function __construct(string $description)
    {
        $this->description = $description;
    }

    public function toString(): string
    {
        return $this->description;
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->toString() === $this->toString();
        }

        return false;
    }
}
