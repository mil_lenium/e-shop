<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

interface ValueObject
{
    public function equals(ValueObject $another): bool;
}
