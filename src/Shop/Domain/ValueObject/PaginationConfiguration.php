<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

final class PaginationConfiguration implements ValueObject, PaginationConfigurationInterface
{
    /**
     * @var string
     */
    private $order;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $limit;

    public static function create(string $order, int $offset, int $limit): self
    {
        return new self($order, $offset, $limit);
    }

    public function __construct(string $order, int $offset, int $limit)
    {
        $this->order = $order;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    public function getOrder(): string
    {
        return $this->order;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->getOrder() === $this->getOrder()
                && $another->getOffset() === $this->getOffset()
                && $another->getLimit() === $this->getLimit();
        }

        return false;
    }
}
