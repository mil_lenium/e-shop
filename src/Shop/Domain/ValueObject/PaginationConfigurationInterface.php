<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

interface PaginationConfigurationInterface
{
    public function getOrder(): string;

    public function getOffset(): int;

    public function getLimit(): int;
}
