<?php

declare(strict_types=1);

namespace App\Shop\Domain\ValueObject;

final class Currency implements ValueObject
{
    private const PLN = 'pln';
    private const USD = 'usd';
    private const EUR = 'eur';

    /**
     * @var string
     */
    private $currency;

    public static function pln(): self
    {
        return new self(self::PLN);
    }

    public static function usd(): self
    {
        return new self(self::USD);
    }

    public static function eur(): self
    {
        return new self(self::EUR);
    }

    public static function fromString(string $currency): self
    {
        return new self($currency);
    }

    private function __construct(string $currency)
    {
        $this->currency = $currency;
    }

    public function toString(): string
    {
        return $this->currency;
    }

    public function isPln(): bool
    {
        return self::PLN === $this->currency;
    }

    public function isUsd(): bool
    {
        return self::USD === $this->currency;
    }

    public function isEUR(): bool
    {
        return self::EUR === $this->currency;
    }

    public function equals(ValueObject $another): bool
    {
        if ($another instanceof self) {
            return $another->toString() === $this->toString();
        }

        return false;
    }
}
