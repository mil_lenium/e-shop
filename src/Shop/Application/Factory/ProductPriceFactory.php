<?php

declare(strict_types=1);

namespace App\Shop\Application\Factory;

use App\Shop\Domain\Factory\ProductPriceFactoryInterface;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\Model\ProductPrice;
use App\Shop\Domain\Model\ProductPriceInterface;
use App\Shop\Domain\ValueObject\Currency;
use App\Shop\Domain\ValueObject\Price;

class ProductPriceFactory implements ProductPriceFactoryInterface
{
    public function create(ProductInterface $product, array $data): ProductPriceInterface
    {
        $price = Price::fromFloat((float) $data['price']);
        $currency = Currency::fromString($data['currency']);

        return new ProductPrice($product, $price, $currency);
    }
}
