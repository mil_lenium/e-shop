<?php

declare(strict_types=1);

namespace App\Shop\Application\Factory;

use App\Shop\Domain\Factory\ProductFactoryInterface;
use App\Shop\Domain\Factory\ProductPriceFactoryInterface;
use App\Shop\Domain\Model\Product;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\ValueObject\ProductDescription;
use App\Shop\Domain\ValueObject\ProductName;
use DateTime;

class ProductFactory implements ProductFactoryInterface
{
    /**
     * @var ProductPriceFactoryInterface
     */
    private $productPriceFactory;

    public function __construct(ProductPriceFactoryInterface $productPriceFactory)
    {
        $this->productPriceFactory = $productPriceFactory;
    }

    public function create(array $data): ProductInterface
    {
        $product = new Product(
            ProductName::fromString($data['name']),
            ProductDescription::fromString($data['description']),
            new DateTime('now')
        );

        foreach ($data['prices'] as $price) {
            $price = $this->productPriceFactory->create($product, $price);
            $product->addPrice($price);
        }

        return $product;
    }
}
