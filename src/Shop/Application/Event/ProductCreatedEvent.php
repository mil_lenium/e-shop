<?php

declare(strict_types=1);

namespace App\Shop\Application\Event;

use App\Shop\Domain\Model\ProductInterface;
use Symfony\Contracts\EventDispatcher\Event;

class ProductCreatedEvent extends Event
{
    public const NAME = 'product.created';

    /**
     * @var ProductInterface
     */
    protected $product;

    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    public function getProduct(): ProductInterface
    {
        return $this->product;
    }
}
