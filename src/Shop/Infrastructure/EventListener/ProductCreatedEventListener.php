<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\EventListener;

use App\Shared\Domain\ValueObject\Message;
use App\Shared\Domain\ValueObject\MessageInterface;
use App\Shared\Infrastructure\Service\MailSenderInterface;
use App\Shop\Application\Event\ProductCreatedEvent;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\Service\Notifier;
use Symfony\Component\Security\Core\Security;
use Traversable;

class ProductCreatedEventListener
{
    /**
     * @var MailSenderInterface
     */
    private $mailSender;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var Notifier[]
     */
    private $notifiers;

    public function __construct(MailSenderInterface $mailSender, Security $security, Traversable $notifiers)
    {
        $this->mailSender = $mailSender;
        $this->security = $security;
        $this->notifiers = iterator_to_array($notifiers);
    }

    public function onProductCreated(ProductCreatedEvent $event): void
    {
        $message = $this->createMessage($event->getProduct());

        foreach ($this->notifiers as $notifier) {
            $notifier->send($message);
        }
    }

    private function createMessage(ProductInterface $product): MessageInterface
    {
        $currentUser = $this->security->getUser()->getUsername();
        $sender = 'admin@somerealdomainhere.com';
        $subject = "Product {$product->getName()->toString()} is created";
        $message = "Product {$product->getName()->toString()} is created, by {$currentUser}";

        return Message::create($subject, $sender, $currentUser, $message);
    }
}
