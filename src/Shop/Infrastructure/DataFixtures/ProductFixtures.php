<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\DataFixtures;

use App\Shop\Domain\Model\Product;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\Model\ProductPrice;
use App\Shop\Domain\ValueObject\Currency;
use App\Shop\Domain\ValueObject\Price;
use App\Shop\Domain\ValueObject\ProductDescription;
use App\Shop\Domain\ValueObject\ProductId;
use App\Shop\Domain\ValueObject\ProductName;
use DateInterval;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    private const DEFAULT_DATE = '2020-01-01 00:00:00';

    public function load(ObjectManager $manager): void
    {
        $createdAt = new DateTime(self::DEFAULT_DATE);
        $description = $this->createProductDescription();

        for ($iterator = 1; $iterator <= 20; $iterator++) {
            $id = ProductId::fromString((string) $iterator);
            $name = ProductName::fromString("Name {$iterator}");

            $product = new Product($name, $description, new DateTime($createdAt->format('Y-m-d h:i:s')), $id);
            $this->setProductPrices($product, $iterator);

            $manager->persist($product);
            $createdAt = $createdAt->add(new DateInterval('P1D'));
        }

        $manager->flush();
    }

    public function createProductDescription(): ProductDescription
    {
        $description = 'That description is nice. ';
        $description .= 'It should contains at least 100 symbols. ';
        $description .= 'It is awesome. Few symbols more please.';

        return ProductDescription::fromString($description);
    }

    public function setProductPrices(ProductInterface $product, int $iterator): void
    {
        $this->addProductPrice($product, 6 * $iterator, Currency::pln());
        $this->addProductPrice($product, 1.5 * $iterator, Currency::eur());
    }

    private function addProductPrice(ProductInterface $product, float $price, Currency $currency): void
    {
        $price = Price::fromFloat($price);
        $price = new ProductPrice($product, $price, $currency);
        $product->addPrice($price);
    }
}
