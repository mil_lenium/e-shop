<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Processor;

use App\Shared\Infrastructure\Service\RequestContentProviderInterface;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Infrastructure\Service\PaginationConfigurationFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class GetProductsProcessor implements GetProductsProcessorInterface
{
    /**
     * @var RequestContentProviderInterface
     */
    private $requestContentProvider;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var PaginationConfigurationFactoryInterface
     */
    private $configurationFactory;

    public function __construct(
        RequestContentProviderInterface $requestContentProvider,
        ProductRepositoryInterface $productRepository,
        PaginationConfigurationFactoryInterface $configurationFactory
    ) {
        $this->requestContentProvider = $requestContentProvider;
        $this->productRepository = $productRepository;
        $this->configurationFactory = $configurationFactory;
    }

    public function process(Request $request): array
    {
        $configuration = $this->configurationFactory->create($request);

        /** @var ProductInterface[] $products */
        $products = $this->productRepository->findList($configuration);
        $total = $this->productRepository->count([]);

        return [
            'products' => array_map(function (ProductInterface $product) {
                return $product->toArray();
            }, $products),
            '_pagination' => [
                'total' => $total,
                'offset' => $configuration->getOffset(),
                'limit' => $configuration->getLimit(),
            ],
        ];
    }
}
