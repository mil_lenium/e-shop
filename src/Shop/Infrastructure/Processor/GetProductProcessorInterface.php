<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Processor;

use App\Shop\Domain\Model\Product;

interface GetProductProcessorInterface
{
    public function process(string $id): ?Product;
}
