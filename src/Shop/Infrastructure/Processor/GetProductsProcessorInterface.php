<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Processor;

use Symfony\Component\HttpFoundation\Request;

interface GetProductsProcessorInterface
{
    public function process(Request $request): array;
}
