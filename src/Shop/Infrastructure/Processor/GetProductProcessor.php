<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Processor;

use App\Shop\Domain\Model\Product;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Domain\ValueObject\ProductId;

class GetProductProcessor implements GetProductProcessorInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function process(string $id): ?Product
    {
        return $this->productRepository->findOneById(ProductId::fromString($id));
    }
}
