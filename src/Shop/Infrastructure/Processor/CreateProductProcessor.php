<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Processor;

use App\Shared\Infrastructure\Service\RequestContentProviderInterface;
use App\Shop\Application\Event\ProductCreatedEvent;
use App\Shop\Domain\Factory\ProductFactoryInterface;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Infrastructure\Validator\CreateProductValidatorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateProductProcessor implements CreateProductProcessorInterface
{
    /**
     * @var RequestContentProviderInterface
     */
    private $requestContentProvider;

    /**
     * @var ProductFactoryInterface
     */
    private $productFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var CreateProductValidatorInterface
     */
    private $validator;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(
        RequestContentProviderInterface $requestContentProvider,
        ProductFactoryInterface $productFactory,
        ProductRepositoryInterface $productRepository,
        CreateProductValidatorInterface $validator,
        EventDispatcherInterface $dispatcher
    ) {
        $this->requestContentProvider = $requestContentProvider;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
    }

    public function process(array $product): ProductInterface
    {
        $this->validator->validate($product);
        $product = $this->productFactory->create($product);
        $this->productRepository->save($product);

        $event = new ProductCreatedEvent($product);
        $this->dispatcher->dispatch($event);

        return $product;
    }
}
