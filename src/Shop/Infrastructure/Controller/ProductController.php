<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Controller;

use App\Shop\Infrastructure\Form\ProductType;
use App\Shop\Infrastructure\Processor\CreateProductProcessorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractController
{
    /**
     * @var CreateProductProcessorInterface
     */
    private $createProductProcessor;

    public function __construct(CreateProductProcessorInterface $createProductProcessor)
    {
        $this->createProductProcessor = $createProductProcessor;
    }

    public function getCreateProductPage()
    {
        $form = $this->createForm(ProductType::class);

        return $this->render('shop/create_product.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function createProduct(Request $request): JsonResponse
    {
        $data = $request->request->get('product');
        $data['prices'] = [];
        $this->createProductProcessor->process($data);

        return JsonResponse::create([], JsonResponse::HTTP_CREATED);
    }
}
