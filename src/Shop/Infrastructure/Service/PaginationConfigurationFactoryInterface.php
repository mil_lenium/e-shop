<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Service;

use App\Shop\Domain\ValueObject\PaginationConfigurationInterface;
use Symfony\Component\HttpFoundation\Request;

interface PaginationConfigurationFactoryInterface
{
    public function create(Request $request): PaginationConfigurationInterface;
}
