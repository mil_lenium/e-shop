<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Service;

use App\Shop\Domain\ValueObject\PaginationConfiguration;
use App\Shop\Domain\ValueObject\PaginationConfigurationInterface;
use Symfony\Component\HttpFoundation\Request;

class PaginationConfigurationFactory implements PaginationConfigurationFactoryInterface
{
    private const DESC_ORDER = 'DESC';
    private const ASC_ORDER = 'ASC';
    private const DEFAULT_OFFSET = 0;
    private const DEFAULT_LIMIT = 10;

    public function create(Request $request): PaginationConfigurationInterface
    {
        $query = $request->query->all();

        return PaginationConfiguration::create(
            $this->getOrder($query),
            $this->getOffset($query),
            $this->getLimit($query)
        );
    }

    private function getOrder(array $query): string
    {
        $order = $query['_order'] ?? self::DESC_ORDER;

        if (self::ASC_ORDER === strtoupper($order)) {
            return self::ASC_ORDER;
        }

        return self::DESC_ORDER;
    }

    private function getOffset(array $query): int
    {
        return (int) ($query['_offset'] ?? self::DEFAULT_OFFSET);
    }

    private function getLimit(array $query): int
    {
        return (int) ($query['_limit'] ?? self::DEFAULT_LIMIT);
    }
}
