<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Validator;

interface CreateProductValidatorInterface
{
    public function validate(array $data): void;
}
