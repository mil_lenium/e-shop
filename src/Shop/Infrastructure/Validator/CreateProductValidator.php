<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Validator;

use App\Shared\Infrastructure\Validator\Validator;
use Symfony\Component\Validator\Constraints\Currency;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Type;

class CreateProductValidator extends Validator implements CreateProductValidatorInterface
{
    public function validate(array $data): void
    {
        $this->validateName($data['name'] ?? null);
        $this->validateDescription($data['description'] ?? null);
        $this->validatePrices($data['prices']);
    }

    private function validateName(?string $name): void
    {
        $this->throwIfNotValid($name, new NotBlank(), 'name');
        $this->throwIfNotValid($name, new Type('string'), 'name');
        $this->throwIfNotValid($name, new Length(['min' => 3]), 'name');
    }

    private function validateDescription(?string $description): void
    {
        $this->throwIfNotValid($description, new NotBlank(), 'description');
        $this->throwIfNotValid($description, new Type('string'), 'description');
        $this->throwIfNotValid($description, new Length(['min' => 100]), 'description');
    }

    private function validatePrices(array $prices): void
    {
        foreach ($prices as $price) {
            $this->validatePriceValue($price['price'] ?? null);
            $this->validateCurrency($price['currency'] ?? null);
        }
    }

    private function validatePriceValue(?float $value): void
    {
        $this->throwIfNotValid($value, new NotBlank(), 'price value');
        $this->throwIfNotValid($value, new Type('float'), 'price value');
        $this->throwIfNotValid($value, new Positive(), 'price value');
    }

    private function validateCurrency(?string $value): void
    {
        $this->throwIfNotValid($value, new NotBlank(), 'currency');
        $this->throwIfNotValid($value, new Type('string'), 'currency');
        $this->throwIfNotValid($value, new Currency(), 'currency');
    }
}
