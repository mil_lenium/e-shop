<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Repository;

use App\Shared\Infrastructure\Repository\EntityRepository;
use App\Shop\Domain\Model\ProductInterface;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Domain\ValueObject\PaginationConfigurationInterface;
use App\Shop\Domain\ValueObject\ProductId;

class ProductRepository extends EntityRepository implements ProductRepositoryInterface
{
    public function save(ProductInterface $product): void
    {
        $this->saveEntity($product);
    }

    public function findOneById(ProductId $id): ?ProductInterface
    {
        return $this->findOneBy(['id' => $id->toString()]);
    }

    public function findList(PaginationConfigurationInterface $configuration): array
    {
        $qb = $this->createQueryBuilder('p');
        $query = $qb
            ->orderBy('p.createdAt', $configuration->getOrder())
            ->setFirstResult($configuration->getOffset())
            ->setMaxResults($configuration->getLimit())
            ->getQuery();

        return $query->getResult();
    }
}
