<?php

declare(strict_types=1);

namespace App\Shop\Infrastructure\Repository;

use App\Shared\Infrastructure\Repository\EntityRepository;
use App\Shop\Domain\Repository\ProductPriceRepositoryInterface;

class ProductPriceRepository extends EntityRepository implements ProductPriceRepositoryInterface
{
}
