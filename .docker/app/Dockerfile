FROM php:7.3-fpm

RUN apt-get update && apt-get install -y --no-install-recommends \
    vim \
    curl \
    debconf \
    wget \
    apt-transport-https \
    apt-utils \
    git \
    build-essential \
    acl \
    mailutils \
    unzip \
    zlib1g-dev

RUN docker-php-ext-install pdo_mysql

COPY .docker/app/php.ini /usr/local/etc/php/php.ini
COPY .docker/app/php-fpm-pool.conf 	/usr/local/etc/php/pool.d/www.conf

COPY . /app
WORKDIR /app

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer

RUN composer install --ignore-platform-reqs

RUN ln -snf /usr/share/zoneinfo/Europe/Lisbon /etc/localtime

RUN mkdir -p var && chown -R www-data:www-data var && chmod -R 777 var
RUN chmod -R 775 /var/www

EXPOSE 9000
CMD ["php-fpm"]
